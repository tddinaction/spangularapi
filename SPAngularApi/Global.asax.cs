﻿using PetProject.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SPAngularApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            SetCurrentDomainData();
            InitializeDb(GetDataSourceString());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void SetCurrentDomainData()
        {
            AppDomain.CurrentDomain.SetData("DirectoryLettersToSend", Server.MapPath("~/Content/LettersToSend/"));
        }

        private string GetDataSourceString()
        {
            return WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString
                                          .Replace("Data Source=", "")
                                          .Replace(@"|DataDirectory|\", Server.MapPath("~/App_Data/"));
        }

        private bool InitializeDb(string dataSourceString)
        {
            try
            {
                using (var db = new SqlRep(dataSourceString))
                {
                    bool needcreate = db.Database.EnsureCreated();
                    if (needcreate)
                    {
                        db.InitializeDataBase();
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);

                return false;
            }
            return true;
        }
    }
}
