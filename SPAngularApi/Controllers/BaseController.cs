﻿using Ninject;
using PetProject.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SPAngularApi.Controllers
{
    public class BaseController : Controller
    {
        protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        [Inject]
        public IDBRepository InjectedDBRepository { get; set; }
    }
}