﻿using Ninject;
using PetProject.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SPAngularApi.Controllers
{
    public class BaseApiController : ApiController
    {
        protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        [Inject]
        public IDBRepository InjectedDBRepository { get; set; }
    }
}
