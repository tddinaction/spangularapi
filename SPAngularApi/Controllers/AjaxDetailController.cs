﻿using PetProject.Models.Repository;
using PetProject.Models.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SPAngularApi.Controllers
{
    public class AjaxDetailController : BaseApiController
    {
        [HttpGet]
        public IEnumerable<Pet> Index(int oid)
        {
            List<Pet> listPet = new List<Pet>();

            try
            {
                listPet = InjectedDBRepository.GetAllPetsByOwnerId(oid);
            }
            catch { }

            return listPet;
        }
    }
}
