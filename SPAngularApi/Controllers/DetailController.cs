﻿using PetProject.Models.Abstract;
using PetProject.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SPAngularApi.Controllers
{
    public class DetailController : BaseController
    {
        public ActionResult Index(int oid)
        {
            // Testing DbContext:
            ViewBag.oid = oid;
            ViewBag.TestDbResultOfOwnerName = InjectedDBRepository.GetOwnerStatView().FirstOrDefault(x => x.OwnerId == oid).Name;

            return View();
        }
    }
}