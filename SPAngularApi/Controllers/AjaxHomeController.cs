﻿using System.Collections.Generic;
using System.Web.Http;
using PetProject.Models.NoSqlEntities;

namespace SPAngularApi.Controllers
{
    public class AjaxHomeController : BaseApiController
    {
        [HttpGet]
        public IEnumerable<OwnerStatView> Index()
        {
            List<OwnerStatView> listOwnerStatView = new List<OwnerStatView>();

            try
            {
                listOwnerStatView = InjectedDBRepository.GetOwnerStatView();
            }
            catch { }

            return listOwnerStatView;
        }
    }
}
