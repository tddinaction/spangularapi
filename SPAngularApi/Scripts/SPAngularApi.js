﻿var SPAngularApi = angular.module('SPAngularApi', ['ngRoute']);

SPAngularApi.controller('HomePageController', HomePageController);
SPAngularApi.controller('DetailPageController', DetailPageController);

var configFunction = function ($routeProvider) {
    $routeProvider.
        when('/Pets/:oid', {
            templateUrl: function (params) { return '/Detail/Index?oid=' + params.oid; }
        })
}
configFunction.$inject = ['$routeProvider'];

SPAngularApi.config(configFunction);